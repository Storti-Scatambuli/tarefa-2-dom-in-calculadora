var notas = []

function verificaNota() {
    var inputnota = document.querySelector('#nota').value
    if (inputnota > 10 || inputnota < 0 || isNaN(inputnota)) {
        alert("Nota inválida, por favor, insita uma nota entre 0 e 10")
    }
    else {
        adicionaNota(inputnota)
    }
}

function adicionaNota(nota) {
    let camponotas = document.querySelector('#campoNotas')
    notas.push(nota)
    camponotas.value += "A nota " + notas.length + " foi: " + nota + "\n"
}

function calculo() {
    if (notas.length != 0) {
        let somatorio = 0
        for (nota of notas) {
            somatorio += parseFloat(nota)
        }
        let media = somatorio / notas.length
        let resultado = document.querySelector('#resultado')
        resultado.innerHTML = "<p>A média é: " + media.toFixed(2) + "</p>"
    }
    else {
        alert('Por favor, insira uma nota para obter a média!')
    }
}
